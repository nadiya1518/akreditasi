<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BakuMutu extends Model
{
    protected $table = 'baku_mutu';
    protected $fillable = ['standar','butir','baku_mutu'];
    protected $primarykey = 'id';

    public $timestamps =false;
}
  