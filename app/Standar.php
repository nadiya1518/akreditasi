<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standar extends Model
{
    protected $table = 'standar';
    protected $fillable = ['kode_standar','nama_standar'];
    protected $primarykey = 'id';

    public $timestamps =false;
}
