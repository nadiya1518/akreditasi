<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LokasiDokumen extends Model
{
    protected $table = 'lokasi_dokumen';
    protected $fillable = ['lokasi_dokumen'];
    protected $primarykey = 'id';

    public $timestamps =false;

}
 