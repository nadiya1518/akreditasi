<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisDokumen;

class JenisdokController extends Controller
{
    public function index()
    {
        $jenis_dokumen = JenisDokumen::all();
        return view('haljenis', ['jenis_dokumen' => $jenis_dokumen]);
    }
    public function jenis()
    {
    	return view ('tjenis');
    }
     public function store(Request $request)
    {
    		JenisDokumen::create([
    		'jenis_dokumen' => $request->jenis_dokumen,
    		]);
    return redirect('/haljenis');
    }
    public function edit($id)
    {
            $jenis_dokumen = JenisDokumen::find($id);
            return view('editjenis', ['jenis_dokumen' => $jenis_dokumen]);
    }

    public function update($id, Request $request)
    {
            $jenis_dokumen = JenisDokumen::find($id);
            $jenis_dokumen->jenis_dokumen = $request->jenis_dokumen;
            $jenis_dokumen->save();
            return redirect('/haljenis');
    }
    public function delete($id)
    {
            $jenis_dokumen = JenisDokumen::find($id);
            $jenis_dokumen->delete();
            return redirect('/haljenis');
    }
}
