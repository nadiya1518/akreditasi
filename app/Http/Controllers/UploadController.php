<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BakuMutu;
use App\LokasiDokumen;
use App\JenisDokumen;

class UploadController extends Controller
{
    public function index()
    {
    	return view ('halamanup');
    }
    public function tambah()
    {
    	$bakumutu=BakuMutu::Orderby("standar","asc")->Orderby('butir',"asc")->get();
    	$lokasidokumen=LokasiDokumen::Orderby("lokasi_dokumen","asc")->get();
    	$jenisdokumen=JenisDokumen::Orderby("jenis_dokumen","asc")->get();
    	return view ('tambah',compact("bakumutu","lokasidokumen","jenisdokumen"));

    }
}
 