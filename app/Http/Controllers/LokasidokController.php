<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LokasiDokumen;

class LokasidokController extends Controller
{
    public function index()
    {
        $lokasi_dokumen = LokasiDokumen::all();
        return view('haldok', ['lokasi_dokumen' => $lokasi_dokumen]);
    }
    public function tlokasi()
    {
    	return view ('tlokasi');
    }
    public function store(Request $request)
    {
    		LokasiDokumen::create([
    		'lokasi_dokumen' => $request->lokasi_dokumen,
    		]);
            return redirect('/haldok');
    }
    public function edit($id)
    {
    $lokasi_dokumen = LokasiDokumen::find($id);
            return view('editlokasi', ['lokasi_dokumen' => $lokasi_dokumen]);
    }
    public function update($id, Request $request)
    {
            $lokasi_dokumen = LokasiDokumen::find($id);
            $lokasi_dokumen->lokasi_dokumen = $request->lokasi_dokumen;
            $lokasi_dokumen->save();

            return redirect('/haldok');
    }
    public function delete($id)
    {
        $lokasi_dokumen = LokasiDokumen::find($id);
        $lokasi_dokumen->delete();
        
        return redirect('/haldok');
    }

}
