<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home','HomeController@index');
Route::get('/st1','SatuController@index');
Route::get('/st2','DuaController@index');
Route::get('/st3','TigaController@index');
Route::get('/st4','EmpatController@index');
Route::get('/st5','LimaController@index');
Route::get('/st6','EnamController@index');
Route::get('/st7','TujuhController@index');
Route::get('/st8','DelapanController@index');
Route::get('/st9','SembilanController@index');

Route::get('/halamanup','UploadController@index');
Route::get('/tambah','UploadController@tambah');

Route::get('/halstan','StandarController@index');
Route::get('/tstandar','StandarController@tst');
Route::post('/tstandar/store', 'StandarController@store');
Route::get('/halstan/edit/{id}', 'StandarController@edit');
Route::put('/halstan/update/{id}', 'StandarController@update');
Route::get('/halstan/hapus/{id}', 'StandarController@delete');


Route::get('/halbaku','BakumutuController@index');
Route::get('/tbaku','BakumutuController@tbaku');
Route::post('/tbaku/store', 'BakumutuController@store');
Route::get('/halbaku/edit/{id}', 'BakumutuController@edit');
Route::put('/halbaku/update/{id}', 'BakumutuController@update');
Route::get('/halbaku/hapus/{id}', 'BakumutuController@delete');


Route::get('/haljenis','JenisdokController@index');
Route::get('/tjenis','JenisdokController@jenis');
Route::post('/tjenis/store', 'JenisdokController@store');
Route::get('/haljenis/edit/{id}','JenisdokController@edit');
Route::put('/haljenis/update/{id}', 'JenisdokController@update');
Route::get('/haljenis/hapus/{id}', 'JenisdokController@delete');



Route::get('/haldok','LokasidokController@index');
Route::get('/tlokasi','LokasidokController@tlokasi');
Route::post('/tlokasi/store', 'LokasidokController@store');
Route::get('/haldok/edit/{id}', 'LokasidokController@edit');
Route::put('/haldok/update/{id}', 'LokasidokController@update');
Route::get('/haldok/hapus/{id}', 'LokasidokController@delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
