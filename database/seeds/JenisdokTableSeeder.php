<?php

use Illuminate\Database\Seeder;

class JenisdokTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    {
    	DB::table('jenis_dokumen')->insert([
         [
        	'jenis_dokumen' => 'Dokumen Wajib'
        ]
          ]);
    }
}
