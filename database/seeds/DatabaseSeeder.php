<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StandarTableSeeder::class);
        $this->call(BakumutuTableSeeder::class);
        $this->call(JenisdokTableSeeder::class);
        $this->call(LokasidokTableSeeder::class);
    }
}
 