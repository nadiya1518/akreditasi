@extends('layouts.backend')

@section('title','Standar 3')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar 3</a></li>
              <li class="breadcrumb-item active">Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
     <div class="card">
        <div class="card-header bg-info">
          <h1 class="card-title bg-info">Mahasiswa</h1>
        </div>

        <div class="card-body">

          <br><a href="/st3/up1"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Latar Belakang</button></td></a><br>
          <a href="/up2"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Kebijakan</button></td></a><br>
          <a href="/up3"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Mekanisme Penetapan & Strategi Pencapaian Standar</button></td></a><br>
          <a href="/up4"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Indikator Kerja Utama</button></td></a><br>
          <a href="/up5"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Indikator Kerja Tambahan</button></td></a><br>
          <a href="/up6"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Evaluasi Capaian Kinerja</button></td></a><br>
          <a href="/up7"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Penjamin Mutu Mahasiswa</button></td></a><br>
          <a href="/up8"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Kepuasan Pengguna</button></td></a><br>
          <a href="/up9"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Kesimpulan Hasil Evaluasi Ketercapaian Standar Kemahasiswaan & Tindaklanjut</button></button></td></a><br>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  @endsection