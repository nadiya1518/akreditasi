<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Dashboard Akreditasi Program Studi|Edit Standar</title>
    </head>

    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>Edit Standar</strong>
                </div>

                <form method="post" action="/halstan/update/{{ $standar->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Kode Standar*</label>
                    <input type="string" name="kode_standar" class="form-control" placeholder="Kode Standar" value=" {{ $standar->kode_standar }} ">

                    @if($errors->has('kodestandar'))
                                <div class="text-danger">
                                    {{ $errors->first('kodestandar')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Nama Standar*</label>
                    <input type="text" name="nama_standar" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $standar->nama_standar }}">

                    @if($errors->has('namastandar'))
                                <div class="text-danger">
                                    {{ $errors->first('namastandar')}}
                                </div>
                     @endif
                    </div>
                
                <!-- /.card-body -->
                  <button type="submit" class="btn btn-primary btn-flat" value="Simpan">Simpan</button>
                  <br>

                  <br><a href="/halstan" class="btn btn-danger btn-flat">Kembali</a>
                  </div>
                  </form>
 
                </div>
            </div>
        </div>
    </body>
</html>