@extends('layouts.backend')

@section('title','Edit Baku Mutu')

@section('content')

<body>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <h5><i class="nav-icon fas fa-list">&ensp;Edit Baku Mutu</i></h5>
          <br>

        <form method="post" action="/halbaku/update/{{ $baku_mutu->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Butir*</label>
                    <input type="string" name="butir" class="form-control" placeholder="Butir" value=" {{ $baku_mutu->butir }} ">

                    @if($errors->has('butir'))
                                <div class="text-danger">
                                    {{ $errors->first('butir')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Baku Mutu*</label>
                    <input type="text" name="baku_mutu" class="form-control" rows="3" placeholder="Baku Mutu" value="{{ $baku_mutu->baku_mutu }}">

                    @if($errors->has('bakumutu'))
                                <div class="text-danger">
                                    {{ $errors->first('bakumutu')}}
                                </div>
                     @endif

                    </div>
                  </div>

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection