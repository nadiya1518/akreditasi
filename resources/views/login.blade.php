<!DOCTYPE html>
<html>
<head>
    <title>HALAMAN LOGIN</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css') }}">
</head>


        <body>
            <div class="flex-center position-ref full-height">
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>

 