@extends('layouts.backend')

@section('title','Standar 2')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar 2</a></li>
              <li class="breadcrumb-item active">Tata Pamong, Tata Kelola, Kerjasama</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header bg-info">
          <h1 class="card-title bg-info">Tata Pamong, Tata Kelola, Kerjasama</h1>
        </div>

        <div class="card-body">
        <td>
         <a href="#"><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Pengelolaan Pembelajaran</button></a><br>
        </td>

        <td>
          <a href="#"><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Pengelolaan Penelitian</button></a><br>
        </td>

        <td>
          <a href="#"><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Pengelolaan PKM</button></a><br>
        </td>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  @endsection