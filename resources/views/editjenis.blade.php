<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Dashboard Akreditasi Program Studi|Edit Jenis Dokumen</title>
    </head>

    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>EDIT Jenis Dokumen</strong>
                </div>

                <form method="post" action="/haljenis/update/{{ $jenis_dokumen->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Dokumen*</label>
                    <input type="string" name="jenis_dokumen" class="form-control" placeholder="Jenis Dokumen" value=" {{ $jenis_dokumen->jenis_dokumen }} ">

                    @if($errors->has('jenisdokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('jenisdokumen')}}
                                </div>
                    @endif
                  </div>
                
                <!-- /.card-body -->
                  <button type="submit" class="btn btn-primary btn-flat" value="Simpan">Simpan</button>
                  <br>

                  <br><a href="/halstan" class="btn btn-danger btn-flat">Kembali</a>
                  </div>
                  </form>
 
                </div>
            </div>
        </div>
    </body>
</html>