@extends('layouts.backend')

@section('title','Standar')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Standar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Standar</i></h5>

            <tr>
              <td>
                <a href="tstandar"><button type="button" class="btn btn-primary btn-small btn-flat"><i class="nav-icon fas fa-plus">&ensp;Tambah Standar</i></button></a><br>
              </td>
            </tr>
      </div>
      <br>
      
      <body>
      <table class="table table-bordered table-hover table-striped">
      <thead>
          <tr>
            <th>Nama Standar</th>
            <th>Aksi</th>
            </tr>
      </thead>
      <tbody>
       @foreach($standar as $b)
         <td>{{ $b->nama_standar }}</td>
         <td>
         <a href="/halstan/edit/{{ $b->id }}" <i class="fas fa-edit btn btn-warning"></i></a>
         <a href="/halstan/hapus/{{ $b->id }}" class="btn btn-danger">Hapus</a>
         </td>
         </tr>
         @endforeach
     </tbody>
     </table>
      </body>
        <!-- /.card-body -->

        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection