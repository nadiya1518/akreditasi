@extends('layouts.backend')

@section('title','Tambah Standar')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar</a></li>
              <li class="breadcrumb-item active">Tambah Standar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Tambah Standar</i></h5>
        </div>
                <form method="post" action="/tstandar/store">
                {{ csrf_field() }} 
                <div class="card-body">
                  <div class="form-group">
                    <label>Kode Standar*</label>
                    <input type="text" name="kode_standar" class="form-control" placeholder="Kode Standar">

                    @if($errors->has('kodestandar'))
                                <div class="text-danger">
                                    {{ $errors->first('kode standar')}}
                                </div>
                    @endif
                    </div>

                    <div class="form-group">
                    <label>Nama Standar*</label>
                    <input type="text" name="nama_standar" class="form-control" placeholder="Nama Standar">

                    @if($errors->has('namastandar'))
                                <div class="text-danger">
                                    {{ $errors->first('nama standar')}}
                                </div>
                    @endif
                    </div>
                
                <!-- /.card-body -->
                  <button type="submit" class="btn btn-primary btn-flat nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                  <br>
                
                <br><tr>
                 <td>
                 <a href="halstan"><button type="button" class="btn btn-danger btn-flat"><nav-icon fas fa-long-arrow-left">Kembali</i></button></a><br>
                 </td>
                </tr>
             </div>
              </form>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection