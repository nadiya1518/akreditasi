@extends('layouts.backend')

@section('title','Tambah Dokumen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Upload Dokumen</a></li>
              <li class="breadcrumb-item active">Tambah Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
 <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Dokumen</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Baku Mutu*</label>
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($bakumutu as $satuanbakumutu)
                      <option>{{ $satuanbakumutu->butir}} {{$satuanbakumutu->baku_mutu}}</option>
                      @endforeach
                    </select>
                </div>

                  <div class="form-group">
                    <label for="exampleInputLokasi1">Lokasi Dokumen*</label>
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($lokasidokumen as $semualokasi)
                      <option>{{ $semualokasi->lokasidokumen}} {{$semualokasi->lokasi_dokumen}}</option>
                      @endforeach
                    </select>
                </div>
                  
                  <div class="form-group">
                    <label for="exampleInputJenis1">Jenis Dokumen*</label>
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>

                      @foreach($jenisdokumen as $semuajenis)
                      <option>{{ $semuajenis->jenisdokumen}} {{$semuajenis->jenis_dokumen}}</option>
                      @endforeach

                    </select>
                  </div>


                  <div class="form-group">
                    <label for="exampleInputFile">Dokumen</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputLink1">Link Url</label>
                    <input type="email" class="form-control" id="exampleInputLink1" placeholder="Masukkan Link Url">
                     <br><tr>
                      <td>
                        <a href="#"><button type="button" class="btn btn-block btn-primary btn-small"><i class="nav-icon fas fa-search">&ensp;Pilih dari daftar dokumen terupload</i></button></a><br>
                      </td>
                    </tr>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputNamadokumen1">Nama Dokumen*</label>
                    <input type="email" class="form-control" id="exampleInputNamadokumen1" placeholder="Masukkan Nama Dokumen">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-flat btn-primary nav-icon fas fa-save">&ensp;Simpan</button>
                </div>
              </form>
            </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection