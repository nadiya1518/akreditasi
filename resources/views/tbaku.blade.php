@extends('layouts.backend')

@section('title','Tambah Baku Mutu')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Referensi</a></li>
              <li class="breadcrumb-item active">Baku Mutu</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Tambah Baku Mutu</i></h5>
      </div>
                <form method="post" action="/tbaku/store">
                {{ csrf_field() }}

                <div class="card-body">
                  <div class="form-group">
                    <label>Standar*</label>
                    <input type="string" name="standar" class="form-control" placeholder="Standar">

                    @if($errors->has('standar'))
                                <div class="text-danger">
                                    {{ $errors->first('standar')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Butir*</label>
                    <input type="string" name="butir" class="form-control" placeholder="Butir">

                    @if($errors->has('butir'))
                                <div class="text-danger">
                                    {{ $errors->first('butir')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Baku Mutu*</label>
                    <input type="text" name="baku_mutu" class="form-control" rows="3" placeholder="Baku Mutu"></textarea>

                    @if($errors->has('bakumutu'))
                                <div class="text-danger">
                                    {{ $errors->first('bakumutu')}}
                                </div>
                    @endif

                    </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>

              <div>
                <tr>
                  <td>
                  <a href="halbaku"><button type="button" class="btn btn-default btn-flat"><nav-icon fas fa-long-arrow-left">Kembali</i></button></a><br>
                  </td>
                </tr>
              </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection