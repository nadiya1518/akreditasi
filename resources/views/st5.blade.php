@extends('layouts.backend')

@section('title','Standar 5')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar 5</a></li>
              <li class="breadcrumb-item active">Keuangan, Sarana, dan Prasarana</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header bg-info">
        <h1 class="card-title bg-info">Keuangan, Sarana, dan Prasarana</h1>
      </div>


        <div class="card-body">
         <br><a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar SarPras Pembelajaran</button></td></a><br>
         <a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Pembiayaan Pembelajaran</button></td></a><br> 
         <a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar SarPras Penelitian</button></td></a><br>
         <a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Pendanaan & Pembiayaan Penelitian</button></td></a><br>
         <a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar SarPras PKM</button></td></a><br>
         <a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Pendanaan & Pembiayaan PKM</button></td></a><br> 
         
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  @endsection