<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Dashboard Akreditasi Program Studi|Edit Lokasi Dokumen</title>
    </head>

    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>Edit Lokasi Dokumen</strong>
                </div>

                <form method="post" action="/haldok/update/{{ $lokasi_dokumen->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Lokasi Dokumen*</label>
                    <input type="string" name="lokasi_dokumen" class="form-control" placeholder="Lokasi Dokumen" value=" {{ $lokasi_dokumen->lokasi_dokumen }} ">

                    @if($errors->has('lokasidokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('lokasidokumen')}}
                                </div>
                    @endif
                  </div>
                
                <!-- /.card-body -->
                  <button type="submit" class="btn btn-primary btn-flat" value="Simpan">Simpan</button>
                  <br>

                  <br><a href="/halstan" class="btn btn-danger btn-flat">Kembali</a>
                  </div>
                  </form>
 
                </div>
            </div>
        </div>
    </body>
</html>