@extends('layouts.backend')

@section('title','Standar 6')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar 6</a></li>
              <li class="breadcrumb-item active">Pendidikan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header bg-info">
        <h1 class="card-title bg-info">Pendidikan</h1>
      </div>

        <div class="card-body">
            <br><a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Isi Pembelajaran</button></td></a><br>
            <a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Proses Pembelajaran</button></td></a><br> 
            <a href="#"><td><button type="button" class="btn btn-block btn-outline-danger btn-lg">Standar Penelitian Pembelajaran</button></td></a><br> 
    
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  @endsection