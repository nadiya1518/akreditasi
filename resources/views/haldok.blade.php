@extends('layouts.backend')

@section('title','Lokasi Dokumen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Referensi</a></li>
              <li class="breadcrumb-item active">Lokasi Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Lokasi Dokumen</i></h5>
            <tr>
              <td>
                <a href="tlokasi"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Lokasi Dokumen</i></button></a><br>
              </td>
            </tr>
      </div>
      <body>   

    <table class="table table-bordered table-hover table-striped">
      <thead>
          <tr>
            <th>Lokasi Dokumen</th>
            <th>Aksi</th>
            </tr>
      </thead>
    <tbody>
       @foreach($lokasi_dokumen as $b)
         <tr>
         <td>{{ $b->lokasi_dokumen }}</td>
         <td>
         <a href="/haldok/edit/{{ $b->id }}"<i class="fas fa-edit btn btn-warning"></i></a>
         <a href="/haldok/hapus/{{ $b->id }}" class="btn btn-danger">Hapus</a>
         </td>
         </tr>
         @endforeach
    </tbody>
    </table>
</body>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection