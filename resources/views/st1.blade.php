@extends('layouts.backend')

@section('title','Standar 1')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar 1</a></li>
              <li class="breadcrumb-item active">Visi, Misi dan Sasaran</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header bg-info">
          <h1 class="card-title">Visi, Misi dan Sasaran</h1>
        </div>
        <div class="card-body">
         <h3>Visi</h3><br>
         <h5> "Menjadi program studi dibidang informatika yang unggul di tingkat nasional dan memiliki reputasi internasional dengan Kekhasan Dalam bidang ICT in Agriculture"</h6><br>
         <h3>Misi</h3><br>
         <h6>1. Menyelenggarakan pendidikan sarjana teknik informatika yang berkualitas internasional.<br>2. Mengembangkan riset-riset unggulan yang berorientasi paten dan terpublikasi internasional.<br>3. Menyelenggarakan kegiatan pengabdian kepada masyarakat terutama dalam penerapan teknologi informasi berbasis hasil penelitian.<br>4. Menjalin dan meningkatkan kerjasama kemitraan yang saling menguntungkan di tingkat lokal, nasional dan internasional</h6><br>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  @endsection